package book

import "encoding/json"

type BookRequest struct {
	Title       string      `json:"title" binding:"required"` //binding for validation
	Price       json.Number `json:"price" binding:"required,number"`
	Rating      int         `json:"rating" binding:"required"`
	Description string      `json:"description" binding:"required"`
}
