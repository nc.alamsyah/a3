package main

import (
	"golang-pustaka-api/book"
	h "golang-pustaka-api/handler"
	"log"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func main() {
	dsn := "root:@tcp(127.0.0.1:3306)/golang_pustaka_api?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal("DB connection error")
	}

	db.AutoMigrate(&book.Book{})

	bookRepository := book.NewRepository(db)
	bookService := book.NewService(bookRepository)
	bookHandler := h.NewBookHandler(bookService)

	// //CRUD
	// //==================
	// //Create Data
	// //==================
	// book := model.Book{}

	// book.Title = "One Piece"
	// book.Price = 750000
	// book.Rating = 99
	// book.Description = "I wanna be The King of Pirates"

	// err = db.Create(&book).Error
	// if err != nil {
	// 	fmt.Println("Error creating book record")
	// }

	// //=================
	// //Read Data
	// //=================
	// var books []model.Book

	// err = db.Debug().Find(&books).Error
	// if err != nil {
	// 	fmt.Println("Can't find books record")
	// }
	// for _, b := range books {
	// 	fmt.Println("Title :", b.Title)
	// 	fmt.Println("book object %v", b)
	// }

	// //=================
	// //Update Data
	// //=================
	// var book model.Book

	// err = db.Debug().Where("id = ?", 1).First(&book).Error
	// if err != nil {
	// 	fmt.Println("Error finding book record")
	// }
	// book.Title = "Harry Potter & the Goblet of Fire"
	// err = db.Save(&book).Error
	// if err != nil {
	// 	fmt.Println("Error updating book record")
	// }

	// //=================
	// //Delete Data
	// //=================
	// var book model.Book

	// err = db.Debug().Where("id = ?", 2).First(&book).Error
	// if err != nil {
	// 	fmt.Println("Error finding book record")
	// }

	// err = db.Delete(&book).Error
	// if err != nil {
	// 	fmt.Println("Error deleting book record")
	// }

	// // Repository layer -> berhubungan dengan DB
	// bookRepository := model.NewRepository(db)

	// // Find All Books
	// books, err := bookRepository.FindAll()
	// if err != nil {
	// 	fmt.Println("Error finding book record")
	// }
	// for _, book := range books {
	// 	fmt.Println("Title :", book.Title)
	// }

	// // Find Book By ID
	// book, err := bookRepository.FindByID(2)
	// if err != nil {
	// 	fmt.Println("Error finding book record")
	// }
	// fmt.Println("Title :", book.Title)

	// // Create Book
	// book := model.Book{
	// 	Title:       "Kungfu Komang",
	// 	Description: "A kungfu comedy comic",
	// 	Price:       20000,
	// 	Rating:      8,
	// }

	// newBook, _ := bookRepository.Create(book)
	// fmt.Println("Success Creating Book", newBook)

	// Service layer -> berhubungan logic/proses bisnis

	router := gin.Default()

	v1 := router.Group("/v1")
	// v1.GET("/", bookHandler.RootHandler)
	// v1.GET("/books", bookHandler.RootHandler)
	// v1.GET("/hello", bookHandler.HelloHandler)
	// v1.GET("/books/:id/:title", bookHandler.BooksHandler)
	// v1.GET("/query", bookHandler.QueryHandler)
	v1.GET("/books", bookHandler.GetBooks)
	v1.GET("/books/:id", bookHandler.GetBook)
	v1.POST("/books", bookHandler.CreateBook)
	v1.PUT("/books/:id", bookHandler.UpdateBook)
	v1.DELETE("/books/:id", bookHandler.DeleteBook)

	router.Run(":8888")
}
